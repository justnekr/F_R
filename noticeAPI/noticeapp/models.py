from django.db import models


class PhoneCode(models.Model):
    def __str__(self):
        return f'{self.id}'

    id = models.PositiveIntegerField(primary_key=True)


class Tag(models.Model):

    def __str__(self):
        return f'{self.id}'
    id = models.CharField(primary_key=True, max_length=100)


class Timezone(models.Model):
    def __str__(self):
        return f'{self.id}'
    id = models.CharField(primary_key=True, max_length=100)


class Sending(models.Model):

    sending_start = models.DateTimeField()
    text = models.TextField()
    client_code = models.ManyToManyField(PhoneCode, related_query_name='sending')
    client_tag = models.ManyToManyField(Tag, related_query_name='sending')
    sending_end = models.DateTimeField()


class Client(models.Model):

    phone_number = models.IntegerField()
    mobile_code = models.ForeignKey(PhoneCode, on_delete=models.CASCADE, unique=False)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE, unique=False)
    tz = models.ForeignKey(Timezone, on_delete=models.CASCADE, unique=False)


class Message(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['client_id', 'sending_id'], name='sending_client'),
        ]

    STATUSES = [
        ('CR', 'created'),
        ('ER', 'error'),
        ('SC', 'success'),
    ]

    sending_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=2, choices=STATUSES, default='CR')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, unique=False)
    sending = models.ForeignKey(Sending, on_delete=models.CASCADE, unique=False)

    def get_text(self):
        return Sending.objects.get(pk=self.sending.id).text

    def get_phone(self):
        return Client.objects.get(pk=self.client.id).phone_number


