import datetime
import zoneinfo

from django.utils import timezone
from rest_framework import serializers
from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import HyperlinkedModelSerializer
from .models import Sending, Message, Client, Tag, PhoneCode, Timezone


class TagModelSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = ('id',)


class PhoneCodeModelSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = PhoneCode
        fields = ('id',)


class SendingModelSerializer(HyperlinkedModelSerializer):
    client_tag = serializers.PrimaryKeyRelatedField(queryset=Tag.objects.all(), many=True)
    client_code = serializers.PrimaryKeyRelatedField(queryset=PhoneCode.objects.all(), many=True)

    class Meta:
        model = Sending
        fields = ('id', 'sending_start', 'sending_end', 'text', 'client_code', 'client_tag')


class ClientModelSerializer(HyperlinkedModelSerializer):
    tag = serializers.PrimaryKeyRelatedField(queryset=Tag.objects.all(), many=False)
    mobile_code = serializers.PrimaryKeyRelatedField(queryset=PhoneCode.objects.all(), many=False)
    tz = serializers.PrimaryKeyRelatedField(queryset=Timezone.objects.all(), many=False)

    class Meta:
        model = Client
        fields = ('id', 'phone_number', 'mobile_code', 'tag', 'tz')


class MessageDataModelSerializer(HyperlinkedModelSerializer):
    text = serializers.CharField(source='get_text')
    phone = serializers.IntegerField(source='get_phone')

    class Meta:
        model = Message
        fields = ('id', 'phone', 'text')


class MessageModelSerializer(HyperlinkedModelSerializer):

    class Meta:
        model = Message
        fields = ('id', 'status')
