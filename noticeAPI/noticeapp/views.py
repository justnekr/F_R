import datetime
import zoneinfo

from django.utils import timezone
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action

from .models import Sending, Message, Client, Tag, PhoneCode
from .serializers import SendingModelSerializer, MessageModelSerializer, ClientModelSerializer, \
    MessageDataModelSerializer


class SendingModelViewSet(ModelViewSet):
    queryset = Sending.objects.all()
    serializer_class = SendingModelSerializer


class ClientModelViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientModelSerializer


class MessageModelViewSet(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageModelSerializer

    @action(detail=False)
    def active(self, request):
        active_messages = []
        active_sending = Sending.objects.filter(
            sending_start__lte=datetime.datetime.now(tz=timezone.get_current_timezone()),
            sending_end__gte=datetime.datetime.now(tz=timezone.get_current_timezone())
        )
        for sending in active_sending:
            codes = PhoneCode.objects.filter(sending__pk=sending.pk)
            tags = Tag.objects.filter(sending__pk=sending.pk)
            clients = Client.objects.filter(
                mobile_code__in=codes,
                tag__in=tags,
            )
            for client in clients:
                message = Message.objects.get_or_create(
                    client=client,
                    sending=sending
                )
                message = message[0]
                if message != 'ST':
                    active_messages.append(message)

        serializer = MessageDataModelSerializer(active_messages, many=True)
        return Response(serializer.data)

