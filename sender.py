import asyncio
import json
import aiohttp as aiohttp
import os
from dotenv import load_dotenv

load_dotenv()

my_token = os.getenv('TOKEN')
incorrect_msgs = []


async def send_status(session, data, status):
    headers = {'Content-Type': 'application/json'}

    url = f"http://127.0.0.1:8000/api/message/{data['id']}/"

    async with session.put(url=url, headers=headers, json={'status': status}) as response:
        print(response)


async def send_msg(session, data):
    headers = {'accept': 'application/json',
               'Authorization': f'Bearer {my_token}',
               'Content-Type': 'application/json'
               }

    url = f"https://probe.fbrq.cloud/v1/send/{data['id']}"

    async with session.post(url=url, headers=headers, json=data) as response:
        if response.ok:
            await send_status(session, data, 'SC')
        else:
            await send_status(session, data, 'ER')


async def sending_msgs():
    # timeout = aiohttp.ClientTimeout(sock_read=None, )

    async with aiohttp.ClientSession() as session:
        headers = {'Content-Type': 'application/json'}
        sending_url = f"http://127.0.0.1:8000/api/message/active/"
        async with session.get(url=sending_url, headers=headers) as response:
            response = await response.text()
            data_list = json.loads(response)
        tasks = []
        for data in data_list:
            task = asyncio.create_task(send_msg(session, data))
            tasks.append(task)

        await asyncio.gather(*tasks)


def main():
    asyncio.run(sending_msgs())


if __name__ == "__main__":
    main()
